const userController = require("./userController");
const carlistController = require("./carlistController");

module.exports = {
  userController,
  carlistController,
};
