'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class carlist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  carlist.init({
    add_user: DataTypes.STRING,
    update_user: DataTypes.STRING,
    delete_user: DataTypes.STRING,
    status_delete: DataTypes.STRING,
    plate: DataTypes.STRING,
    manufacture: DataTypes.STRING,
    model: DataTypes.STRING,
    image: DataTypes.STRING,
    rentperday: DataTypes.INTEGER,
    capacity: DataTypes.STRING,
    desc: DataTypes.STRING,
    transmission: DataTypes.STRING,
    type: DataTypes.STRING,
    year: DataTypes.STRING,
    options: DataTypes.STRING,
    specs: DataTypes.STRING,
    availableat: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'carlist',
  });
  return carlist;
};